import math

# ti - transmitting i
# tn_wasi - transmitting noise, last correct transmission state was ti
states = ('t0', 't1', 'tn_was0', 'tn_was1')
  
start_probability = {'t0': 0.5, 't1': 0.5, 'tn_was0':0.0, 'tn_was1':0.0}
 
transition_probability = {
	't0': {'t0':0.9*0.7, 't1':0.9*0.3, 'tn_was0':0.1, 'tn_was1':0.0}, 
	't1': {'t0':0.9*0.1, 't1':0.9*0.9, 'tn_was0':0.0, 'tn_was1':0.1}, 
	'tn_was0': {'t0':0.8*0.7, 't1':0.8*0.3, 'tn_was0':0.2, 'tn_was1':0.0}, 
	'tn_was1': {'t0':0.8*0.1, 't1':0.8*0.9, 'tn_was0':0.0, 'tn_was1':0.2}
   }
 
emission_probability = {
	't0': {'0':0.8, '1':0.2}, 
	't1': {'0':0.2, '1':0.8}, 
	'tn_was0': {'0':0.5, '1':0.5}, 
	'tn_was1': {'0':0.5, '1':0.5}, 
   }

# deal with 0 probabilities
def loglikelihood(p):
	return math.log(p) if p != 0 else float("-infinity")

def print_dptable(V):
	s = "    " + " & ".join(("%7d" % i) for i in range(len(V))) + "\n"
	for y in V[0]:
		s += "%.25s & " % y
		s += " & ".join("%.9s" % ("%.17f" % v[y]) for v in V)
		s += "\\\\\n"
	print(s)

def forward(obs, states, start_p, trans_p, emit_p):
	V = [{}]
	path = {}
 
	# Initialize base cases (t == 0)
	for y in states:
		V[0][y] = loglikelihood(start_p[y]) + loglikelihood(emit_p[y][obs[0]])
 
	# Run Forward for t > 0
	for t in range(1, len(obs)):
		V.append({})
 
		for y in states:
			m = max(V[t-1][y0] + loglikelihood(trans_p[y0][y]) for y0 in states)
			prob = loglikelihood(sum(math.exp(V[t-1][y0] + loglikelihood(trans_p[y0][y]) - m) for y0 in states)) + m + loglikelihood(emit_p[y][obs[t]])
			V[t][y] = prob
	n = 0           # if only one element is observed max is sought in the initialization values
	if len(obs) != 1:
		n = t
	print_dptable(V)
	m = max(V[n][y] for y in states)
	prob = loglikelihood(sum(math.exp(V[n][y]) for y in states))
	return prob

print forward(tuple('11011000'), states, start_probability, transition_probability, emission_probability)