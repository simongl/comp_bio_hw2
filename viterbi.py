import copy
import math
import operator
import itertools
import padnums
import sys

# deal with 0 probabilities
def loglikelihood(p):
    return math.log(p) if p != 0 else float("-infinity")


def prettyprint_dptable(V):
    h = [[' '] + [str(i) for i in xrange(len(V))]]
    rows = [[s] + [str(V[i][s]) for i in xrange(len(V))] for s in V[0].keys()]
    r = h + rows
    padnums.pprint_table(sys.stdout, h + rows)


def viterbi(obs, states, start_p, trans_p, emit_p):
    V = [{}]
    path = {}

    # Initialize base cases (t == 0)
    for y in states:
        V[0][y] = loglikelihood(start_p[y]) + loglikelihood(emit_p[y][obs[0]])
        path[y] = [y]

    # Run Viterbi for t > 0
    for t in range(1, len(obs)):
        V.append({})
        newpath = {}

        for y in states:
            (prob, state) = max((V[t - 1][y0] + loglikelihood(trans_p[y0][y]) + loglikelihood(emit_p[y][obs[t]]), y0) for y0 in states)
            V[t][y] = prob
            newpath[y] = path[state] + [y]

        # Don't need to remember the old paths
        path = newpath
    n = 0           # if only one element is observed max is sought in the initialization values
    if len(obs) != 1:
        n = t
    #print_dptable(V)
    prettyprint_dptable(V)
    (prob, state) = max((V[n][y], y) for y in states)
    return prob, path[state]

#print viterbi(tuple('11011000'), states, start_probability, transition_probability, emission_probability)


def transition_count(S, s1, s2):
    return sum([1 if (first == s1 and second == s2) else 0 for (first, second) in zip(S, S[1:])])


def emission_count(S, X, s, e):
    return sum([1 if (first == s and second == e) else 0 for (first, second) in zip(S, X)])


def posterior_likelihood(S, X, T, E, alphabet):
    transmissions_product = reduce(operator.mul, [T[s1][s2] ** transition_count(S, s1, s2) for (s1, s2) in itertools.product(T.keys(), T.keys())], 1)
    emissions_product = reduce(operator.mul, [E[s][e] ** transition_count(S, s, e) for (s, e) in itertools.product(T.keys(), alphabet)], 1)
    return transmissions_product * emissions_product


def update_model(S, X, transition_probability, emission_probability, start_probability, states, alphabet):
    appearance_counts = dict(((state, len([s for s in S if s == state])) for state in states))
    for (s1, s2) in itertools.product(states, states):
        tcount = float(transition_count(S, s1, s2))
        acount = appearance_counts[s1]
        transition_probability[s1][s2] = 0.001 if acount == 0 else tcount / acount
    for s in states:
        for c in alphabet:
            ecount = float(emission_count(S, X, s, c))
            acount = appearance_counts[s]
            emission_probability[s][c] = 0.001 if acount == 0 else ecount / acount


def dump_model(transition_probability, emission_probability, start_probability, states, alphabet):
    padnums.pprint_table(sys.stdout, zip(*[('{s1}->{s2}'.format(s1=s1, s2=s2), str(transition_probability[s1][s2])) for (s1, s2) in itertools.product(states, states)]))


def dump_model_history(history, states, alphabet):
    h = ['{s1}->{s2}'.format(s1=s1, s2=s2) for (s1, s2) in itertools.product(states, states)]
    t = [h]
    for (transition_probability, emission_probability, start_probability) in history:
        curline = [str(transition_probability[s1][s2]) for (s1, s2) in itertools.product(states, states)]
        t.append(curline)
    padnums.pprint_table(sys.stdout, t)


def infer_parameters(X, states, alphabet, initial_transmissions, initial_emissions, initial_starts):
    # initial model parameters: uniform distribution
    transition_probability = initial_transmissions
    emission_probability = initial_emissions
    start_probability = initial_starts

    model_history = [[copy.deepcopy(transition_probability), copy.deepcopy(emission_probability), copy.deepcopy(start_probability)]]
    old_likelihood = 0
    epsilon = 0.0001

    while True:
        (likelihood, S) = viterbi(X, states, start_probability, transition_probability, emission_probability)
        print "current annotation {S}".format(S=S)
        if abs(old_likelihood - likelihood) < epsilon:
            break
        old_likelihood = likelihood
        update_model(S, X, transition_probability, emission_probability, start_probability, states, alphabet)
        model_history.append([copy.deepcopy(transition_probability), copy.deepcopy(emission_probability), copy.deepcopy(start_probability)])

    dump_model_history(model_history, states, alphabet)


states = ['A', 'B', 'C', 'D']
alphabet = ['0', '1']

X = '1010101101010101'

#v = viterbi(X, states, start_probability, transition_probability, emission_probability)
#print 'score: {p}'.format(p=v[0])

# according to the example in the assignment
initial_transition_probability = {
    'A': {'A': 0.8, 'B': 0.1, 'C': 0.1, 'D': 0.0},
    'B': {'A': 0.1, 'B': 0.6, 'C': 0.0, 'D': 0.3},
    'C': {'A': 0.8, 'B': 0.1, 'C': 0.1, 'D': 0.0},
    'D': {'A': 0.1, 'B': 0.8, 'C': 0.0, 'D': 0.1}
   }

initial_emission_probability = {
    'A': {'0': 0.9, '1': 0.1},
    'B': {'0': 0.1, '1': 0.9},
    'C': {'0': 0.5, '1': 0.5},
    'D': {'0': 0.5, '1': 0.5},
   }

initial_start_probability = {'A': 0.5, 'B': 0.5, 'C':0.0, 'D':0.0}

infer_parameters(X, states, alphabet, initial_transition_probability, initial_emission_probability, initial_start_probability)